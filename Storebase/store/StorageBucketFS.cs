﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml.Serialization;
using Storebase.util;
using SPath = System.IO.Path;

namespace Storebase.store {
    public partial class StorageBucket {
        private FileSystemWatcher _watcher;

        [XmlIgnore]
        public EventWaitHandle ChangedHandle { get; } = new EventWaitHandle(false, EventResetMode.ManualReset);

        // ReSharper disable once FieldCanBeMadeReadOnly.Global
        public SerializableDictionary<string, FileData> SimpleChangeList =
            new SerializableDictionary<string, FileData>();

        public void CheckOnceForChanges(bool isRemote) {
            if (!Directory.Exists(Path))
                Directory.CreateDirectory(Path);
            
            Settings.IsRemote = isRemote;

            var existingFiles = new List<string>();
            void IterateDirectory(string pathname) {
                
                // loop though all files in the bucket directory and note changes
                foreach (var fse in Directory.EnumerateFileSystemEntries(pathname)) {
                    var dir = File.GetAttributes(fse).HasFlag(FileAttributes.Directory);
                    
                    var brp = MakeFilepathRelative(fse);
                    SimpleChangeList[brp] = new FileData {
                        Hash = dir ? "dir" : FileUtils.GetMD5(fse),
                        ChangeTime = File.GetLastWriteTime(fse).Ticks,
                        IsRemote = isRemote
                    };
                        
                    existingFiles.Add(brp);
                    
                    if (dir) IterateDirectory(fse);
                }
            }
            IterateDirectory(Path);
            
            // files that are in the list but not in the filesystem have been deleted (obviously)
            foreach (var key in SimpleChangeList.Keys.Except(existingFiles)) {
                var dir = SimpleChangeList[key].Hash.Equals("dir");
                
                SimpleChangeList[key] = new FileData {
                    Hash = dir ? "rmdir" : "rm",
                    ChangeTime = DateTime.Now.Ticks, // todo: Is there a way to get the corrrect value here?
                    IsRemote = isRemote
                };
            }
        }
        
        // ReSharper disable once InconsistentNaming
        public void EnableFsw(bool isRemote, bool autoEnable = true) {
            if (!Directory.Exists(Path))
                Directory.CreateDirectory(Path);
            
            _watcher = new FileSystemWatcher(Path) {
                EnableRaisingEvents = true,
                IncludeSubdirectories = true
            };

            Settings.IsRemote = isRemote;
            Settings.AutoStartFsw = autoEnable;

            var listener = new FileSystemEventHandler((_, args) => {
                try {
                    var dir = File.GetAttributes(args.FullPath).HasFlag(FileAttributes.Directory);
                    var brp = MakeFilepathRelative(args.FullPath);

                    Thread.Sleep(250);
                    switch (args.ChangeType) {
                        case WatcherChangeTypes.Created:
                            SimpleChangeList.Add(brp,
                                new FileData {
                                    Hash = dir ? "dir" : FileUtils.GetMD5(args.FullPath),
                                    ChangeTime = File.GetLastWriteTime(args.FullPath).Ticks,
                                    IsRemote = isRemote
                                });
                            break;
                        case WatcherChangeTypes.Deleted:
                            SimpleChangeList[brp] = new FileData {
                                Hash = dir ? "rmdir" : "rm",
                                ChangeTime = DateTime.Now.Ticks,
                                IsRemote = isRemote
                            };
                            break;
                        case WatcherChangeTypes.Changed:
                            SimpleChangeList[brp] = new FileData {
                                Hash = dir ? "dir" : FileUtils.GetMD5(args.FullPath),
                                ChangeTime = File.GetLastWriteTime(args.FullPath).Ticks,
                                IsRemote = isRemote
                            };
                            break;
                    }
                    ChangedHandle.Set();
                } catch (Exception e) {
                    // most likely "error opening file"
                    // just keep the old data in that case
                }
            });

            _watcher.Changed += listener;
            _watcher.Created += listener;
            _watcher.Deleted += listener;
            _watcher.Renamed += (_, args) => {
                var dir = File.GetAttributes(args.FullPath).HasFlag(FileAttributes.Directory);
                var obrp = MakeFilepathRelative(args.OldFullPath);
                var brp = MakeFilepathRelative(args.FullPath);

                if (SimpleChangeList.ContainsKey(obrp)) {
                    var ofh = SimpleChangeList[obrp];
                    SimpleChangeList.Remove(obrp);
                    SimpleChangeList.Add(brp, ofh);
                } else {
                    Thread.Sleep(250);

                    SimpleChangeList.Add(brp,
                        new FileData {
                            Hash = dir ? "dir" : FileUtils.GetMD5(args.FullPath),
                            ChangeTime = File.GetLastWriteTime(args.FullPath).Ticks,
                            IsRemote = isRemote
                        });
                }
                ChangedHandle.Set();
            };
        }
    }
}
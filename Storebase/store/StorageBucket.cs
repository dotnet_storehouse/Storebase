﻿using System;
using System.Collections.Generic;
using System.Linq;
using Storebase.net.@base;
using SPath = System.IO.Path;

namespace Storebase.store {
    public partial class StorageBucket {
        public Guid Guid;
        public string Name;
        // writability and publicity required for xml deserialization
        // ReSharper disable once MemberCanBePrivate.Global
        // ReSharper disable once FieldCanBeMadeReadOnly.Global
        public StorageBucketSettings Settings = new StorageBucketSettings();
        
        private string _path;

        // The path will always be absolute and will end with the System.IO.Path.DirectorySeparatorChar.
        public string Path {
            get => _path;
            set {
                var tmp = SPath.GetFullPath(value);
                if (!tmp.EndsWith(System.IO.Path.DirectorySeparatorChar + ""))
                    tmp = tmp + System.IO.Path.DirectorySeparatorChar;
                
                if (_watcher != null) _watcher.Path = tmp;
                _path = tmp;
            }
        }

        public StorageBucket() {
            Guid = Guid.NewGuid();
        }

        public void PostDeserialize() {
            if (Settings.AutoStartFsw) {
                EnableFsw(Settings.IsRemote);
            }
        }

        public string MakeFilepathAbsolute(string relativePath) {
            var _base = new Uri(Path, UriKind.Absolute);
            var u1 = new Uri(_base, relativePath);
            return Uri.UnescapeDataString(u1.AbsolutePath);
        }

        public string MakeFilepathRelative(string absolutePath) {
            var _base = new Uri(Path, UriKind.Absolute);
            var file = new Uri(SPath.GetFullPath(absolutePath));
            return Uri.UnescapeDataString(_base.MakeRelativeUri(file).ToString());
        }
        
        public override string ToString() {
            return $"{Name} [{Path}] {{{Guid}}}";
        }
    }

    public static class StorageBucketUtils {
        public static List<StorageBucket> GetBucketsWithGuidsLike(this List<StorageBucket> buckets, string guid) {
            return buckets.Where(bckt => bckt.Guid.ToString().StartsWith(guid)).ToList();
        }

        public static (Signal.Codes, StorageBucket) TryGetBucketWithGuidLike(this List<StorageBucket> buckets, string guid) {
            var bl = buckets.GetBucketsWithGuidsLike(guid);
            if (bl.Count < 1) {
                Console.Error.WriteLine("Bucket not found!");
                return (Signal.Codes.ErrorBucketNotFound, null);
            }

            if (bl.Count > 1) {
                Console.Error.WriteLine("Ambiguous GUID given.");
                return (Signal.Codes.ErrorAmbiguousGuid, null);
            }

            return (Signal.Codes.Ack, bl[0]);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Xml.Serialization;

namespace Storebase.store {
    public class FileTransferHeader {
        public bool ClientSend { get; set; }

        [XmlIgnore]
        public bool ServerSend {
            get => !ClientSend;
            set => ClientSend = !value;
        }

        public Guid BucketGuid;
        public string Filename;

        public long Index = -1;
        [XmlIgnore]
        public List<(int index, byte[] data)> Segments = new List<(int index, byte[] data)>();

        public override bool Equals(object obj) {
            if (!(obj is FileTransferHeader fth)) return false;
            
            return ClientSend == fth.ClientSend &&
                BucketGuid.Equals(fth.BucketGuid) &&
                Filename.Equals(fth.Filename);
        }
    }
}
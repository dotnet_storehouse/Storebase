﻿using System;
using System.Net;
using System.Net.Sockets;
using Storebase.net.@base;
using TcpClient = Storebase.net.@base.TcpClient;

namespace Storebase.net.connectivity {
    public partial class ConHost {

        public static TcpClient Connect(string hostname, int port, string certificatePath) {
	        try {
		        var c = new TcpClient(hostname, port);
		        c.InitSslAsClient(certificatePath);
		        return c;
	        } catch (SocketException e) {
				Console.Error.Write(e.Message);
				return null;
	        }
        }

	    public static TcpClient AwaitSingleConnection(int port, string certPath) {
		    var listener = new TcpListener(IPAddress.Any, port);
		    listener.Start(1);
		    return listener.AcceptTcpClient_(certPath);
	    }
        
    }
}
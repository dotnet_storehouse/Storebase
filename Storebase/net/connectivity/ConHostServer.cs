﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Storebase.net.@base;
using TcpClient = Storebase.net.@base.TcpClient;

namespace Storebase.net.connectivity {
    public partial class ConHost {

        private readonly TcpListener _listener;
        private Thread _connThread;

        private readonly long _maxClients;
        private readonly string _certificatePath;
        private readonly List<TcpClient> _clients = new List<TcpClient>();
        public int ConnectedClients => _clients.Count(client => client.Connected);

        public event EventHandler<TcpClient> OnConnect;
        
        public ConHost(string certificatePath, int port, long maxClients = long.MaxValue) {
            _listener = new TcpListener(IPAddress.Any, port);
            _maxClients = maxClients;
            _certificatePath = certificatePath;
        }

        public void Start() {
            _listener.Start(64);
            
            _connThread = new Thread(() => {
                while (true) {
                    while (ConnectedClients > _maxClients) Thread.Sleep(100);
                    var client = _listener.AcceptTcpClient_(_certificatePath);
                    _clients.Add(client);
                    OnConnect?.Invoke(this, client);
                }
            });
            _connThread.Start();
        }

        public void Stop() {
            _connThread.Abort();
            _connThread = null;
            _listener.Stop();
        }
        
    }
}
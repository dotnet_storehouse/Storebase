﻿using Storebase.net.messageloop;

namespace Storebase.net.@base {
    public interface IMessageHandler {

        Signal.Type HandeledType { get; }
        Signal.Codes HandeledCode { get; }

        void OnMessage(ref Message m, BasicMessageLoop loop);

    }
}
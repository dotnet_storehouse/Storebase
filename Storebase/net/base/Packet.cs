﻿namespace Storebase.net.@base {
	public struct Packet {
		public int Length => Data.Length;
		public byte Type;
		public long MessageId;
		public byte[] Data;

		public Packet(Signal.Type type, byte[] data = null) {
			if (data != null) {
				Data = data;
			} else
				Data = new byte[0];
			Type = (byte) type;
			MessageId = 0;
		}

		public Packet(Signal.Type type, Signal.Codes code) {
			Data = new[] {(byte) code};

			Type = (byte) type;
			MessageId = 0;
		}
		
		public Message ToMsg() => new Message {Request = this};
	}
}

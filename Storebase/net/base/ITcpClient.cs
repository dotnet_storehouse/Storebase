﻿namespace Storebase.net.@base {
    public interface ITcpClient {
        bool Online { get; }
        int Available { get; }
        
        string Hostname { get; }
        int Port { get; }
        string CertificatePath { get; }

        void SendPacket(Packet p);
        Packet? ReadPacket();
        
        void Close();
    }
}
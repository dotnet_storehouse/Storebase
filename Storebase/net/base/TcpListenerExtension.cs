﻿using System.Net.Sockets;

namespace Storebase.net.@base {
    public static class TcpListenerExtension {
        public static TcpClient AcceptTcpClient_(this TcpListener @this, string certificatePath) {
            var c = new TcpClient(@this.AcceptSocket());
            c.InitSslAsServer(certificatePath);
            return c;
        }
    }
}
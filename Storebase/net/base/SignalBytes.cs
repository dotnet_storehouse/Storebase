﻿namespace Storebase.net.@base {
	public static class Signal {
		public enum Type : byte {
			Control = 0x1,
			Data = 0x2,
			FileTransferHeader = 0x3,
			FileTransferData = 0x4
		}

		public enum Codes : byte {
			Error							= 0x00,
			ErrorBucketNotFound				= 0x01,
			ErrorDuplicateBucket			= 0x02,
			ErrorAmbiguousGuid				= 0x03,
			ErrorTransmissionDenied 		= 0x04,
			ErrorSegmentsLostAndDiscarded	= 0x05,
			ErrorSegmentsLostButRecoverable = 0x06,
			
			Ack								= 0x10,
			
			HeartBeat						= 0x20,
			
			BucketList						= 0x30,
			BucketPush						= 0x31,
			BucketFetch						= 0x32,
			BucketGet						= 0x34,
			
			TransferStart					= 0x41,
			TransferFinished				= 0x42,
		}
	}
}
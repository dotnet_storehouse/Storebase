﻿using System;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Reflection;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;

namespace Storebase.net.@base {
    public class TcpClient : System.Net.Sockets.TcpClient, ITcpClient {
        // ReSharper disable once MemberCanBePrivate.Global
        public bool IsDisposed {
            get {
                var field = typeof(System.Net.Sockets.TcpClient).GetField("m_CleanedUp",
                    BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetField);
                return (bool) (field?.GetValue(this) ?? false);
            }
        }

        public bool Online => Connected && !IsDisposed;

        private SslStream SecureStream { get; set; }
        public string CertificatePath { get; private set; }

        public string Hostname { get; }
        public int Port { get; }

        // Used by the server
        public TcpClient(Socket s) {
            Client = s;

            var field = typeof(System.Net.Sockets.TcpClient).GetField("m_Active",
                BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetField);
            field?.SetValue(this, true);
            //m_Active = true;

            Hostname = ((IPEndPoint) s.RemoteEndPoint).Address.ToString();
            Port = ((IPEndPoint) s.LocalEndPoint).Port;
        }

        // Used by the client(s)
        public TcpClient(string hostname, int port) : base(hostname, port) {
            Hostname = hostname;
            Port = port;
        }

        public void InitSslAsServer(string certificatePath) {
            CertificatePath = certificatePath;
            SecureStream = new SslStream(GetStream(), true, (_, cert, chain, errors) => errors == SslPolicyErrors.None);

            var serverCert = new X509Certificate2(CertificatePath, "");

            SecureStream.AuthenticateAsServer(serverCert, true, SslProtocols.Tls, false);
        }

        public void InitSslAsClient(string certificatePath) {
            CertificatePath = certificatePath;
            SecureStream = new SslStream(GetStream(), true, (_, cert, chain, errors) => errors == SslPolicyErrors.None);

            var clientCert = new X509Certificate2(CertificatePath, "");

            SecureStream.AuthenticateAsClient("Storehouse Storekeeper", new X509Certificate2Collection(clientCert),
                SslProtocols.Tls, false);
        }

        private byte[] ReadNBytes(int length) {
            var buf = new byte[length];
            var read = 0;
            while (read < length) read += SecureStream.Read(buf, read, length - read);
            return buf;
        }

        public Packet? ReadPacket() {
            try {
                var p = new Packet();

                var length = BitConverter.ToInt32(ReadNBytes(sizeof(int)), 0);
                p.Type = (byte) SecureStream.ReadByte();
                p.MessageId = BitConverter.ToInt64(ReadNBytes(sizeof(long)), 0);
                p.Data = ReadNBytes(length);

                return p;
            } catch { return null; }
        }

        public void SendPacket(Packet p) {
            try {
                var buf = BitConverter.GetBytes(p.Length);
                SecureStream.Write(buf, 0, buf.Length);
                SecureStream.WriteByte(p.Type);
                buf = BitConverter.GetBytes(p.MessageId);
                SecureStream.Write(buf, 0, buf.Length);
                SecureStream.Write(p.Data, 0, p.Data.Length);
            } catch {
                // ignored
            }
        }
    }
}
﻿using System;
using System.Threading;

namespace Storebase.net.@base {
    public class Message {
        public Packet Request;

        private Packet _response;

        public Packet Response {
            get => _response;
            set {
                _response = value;
                _response.MessageId = Request.MessageId;

                OnResponse?.Invoke(this, this);

                ResponseHandle.Set();
            }
        }

        public EventWaitHandle ResponseHandle { get; } = new EventWaitHandle(false, EventResetMode.ManualReset);

        public event EventHandler<Message> OnResponse;
    }
}
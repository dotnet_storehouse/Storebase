﻿using System.Timers;
using Storebase.net.@base;

namespace Storebase.net.messageloop {
    public class HeartBeatSender {
        
        private Timer _timer;
        private BasicMessageLoop _loop;

        public HeartBeatSender(BasicMessageLoop loop) {
            _loop = loop;
            
            _timer = new Timer();
            _timer.Elapsed += (_, args) => {
                if (!_loop.IsRunning) {
                    _timer.Stop();
                    return;
                }

                _loop.PostMessage(new Packet {
                    Type = (byte) Signal.Type.Control,
                    Data = new[] {(byte) Signal.Codes.HeartBeat}
                }.ToMsg());
            };
        }

        public void Start() {
            _timer.Start();
        }
    }
}
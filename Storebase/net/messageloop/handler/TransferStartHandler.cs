﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Storebase.net.@base;
using Storebase.store;

namespace Storebase.net.messageloop.handler {
    public class TransferStartHandler : IMessageHandler {
        public Signal.Type HandeledType => Signal.Type.Control;
        public Signal.Codes HandeledCode => Signal.Codes.TransferStart;

        public void OnMessage(ref Message m, BasicMessageLoop loop) {
            var index = BitConverter.ToInt64(m.Request.Data, 1);

            if (!(loop is StorehouseMessageLoop sw)) return;

            if (!sw.IsServer) {
                var head = sw.ActiveFileTransfers.First(fth => fth.Index == index);
                sw.FileTransferMessageLoop.SendFileData(head);
            }

            m.Response = new Packet(Signal.Type.Control, Signal.Codes.Ack);
        }
    }
}
﻿using Storebase.net.@base;

namespace Storebase.net.messageloop.handler {
    public class HeartBeatMessageHandler : IMessageHandler {
        public Signal.Type HandeledType => Signal.Type.Control;

        public Signal.Codes HandeledCode => Signal.Codes.HeartBeat;
        
        public void OnMessage(ref Message m, BasicMessageLoop loop) {
            m.Response = new Packet(Signal.Type.Control, Signal.Codes.Ack);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Storebase.net.@base;
using Storebase.store;

namespace Storebase.net.messageloop.handler {
    public class TransferFinishHandler : IMessageHandler {
        public Signal.Type HandeledType => Signal.Type.Control;
        public Signal.Codes HandeledCode => Signal.Codes.TransferFinished;

        public void OnMessage(ref Message m, BasicMessageLoop loop) {
            var index = BitConverter.ToInt64(m.Request.Data, 1);
            var segCount = BitConverter.ToInt32(m.Request.Data, 1 + sizeof(long));

            if (!(loop is FileTransferMessageLoop fl)) return;
            var sw = fl.Parent;

            var head = sw.ActiveFileTransfers.First(fth => fth.Index == index);
            if (head.Segments.Count != segCount) {
                // todo: report what was received and what not.
                m.Response = new Packet(Signal.Type.Control, Signal.Codes.ErrorSegmentsLostAndDiscarded);
                head.Segments.Clear();
                return;
            }

            head.Segments.Sort((t1, t2) => t1.index.CompareTo(t2.index));

            IEnumerable<byte> data = new byte[0];
            foreach (var s in head.Segments) data = data.Concat(s.data);

            var bucket = sw.GetBucketList().First(b => b.Guid.Equals(head.BucketGuid));

            sw.File.WriteAllBytes(bucket.MakeFilepathAbsolute(head.Filename), data.ToArray());

            sw.ActiveFileTransfers.Remove(head);
            
            m.Response = new Packet(Signal.Type.Control, Signal.Codes.Ack);
        }
    }
}
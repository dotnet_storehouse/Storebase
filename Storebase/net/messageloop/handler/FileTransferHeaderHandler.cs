﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Storebase.net.@base;
using Storebase.store;

namespace Storebase.net.messageloop.handler {
    public class FileTransferHeaderHandler : IMessageHandler {
        public Signal.Type HandeledType => Signal.Type.FileTransferHeader;
        public Signal.Codes HandeledCode => Signal.Codes.Ack;

        public void OnMessage(ref Message m, BasicMessageLoop loop) {
            var str = new MemoryStream(m.Request.Data.Skip(1).ToArray());
            var ser = new XmlSerializer(typeof(FileTransferHeader));
            var head = (FileTransferHeader) ser.Deserialize(str);
            str.Close();

            if (!(loop is StorehouseMessageLoop sw)) return;

            var (valid, _) = sw.QueueFileTransfer(head, true);

            m.Response = valid
                ? new Packet(Signal.Type.Control, Signal.Codes.Ack)
                : new Packet(Signal.Type.Control, Signal.Codes.ErrorTransmissionDenied);
        }
    }
}
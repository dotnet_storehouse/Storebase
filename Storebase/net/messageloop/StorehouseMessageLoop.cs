﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Storebase.net.@base;
using Storebase.net.connectivity;
using Storebase.net.messageloop.handler;
using Storebase.store;
using Storebase.util;

/*
 * Contains logic for file transfers.
 * Server has priority:
 *   - clients can request transfers, but not actually start them
 *   - server may start a pending transfer at any time, usually in accordance with his available resources
 */
namespace Storebase.net.messageloop {
    public abstract class StorehouseMessageLoop : BasicMessageLoop {
        protected internal readonly bool IsServer;
        protected internal FileTransferMessageLoop FileTransferMessageLoop;
        protected internal IFileTestInterface File = new FileTestInterface();

        protected long _fthIndex;
        private long _fthIndexStep = 2;

        public readonly List<FileTransferHeader> ActiveFileTransfers = new List<FileTransferHeader>();
        public readonly List<FileTransferHeader> PendingFileTransfers = new List<FileTransferHeader>();

        protected StorehouseMessageLoop(ITcpClient client, bool isServer) :
            base(client, "SHML @ " + (isServer ? "server" : "client")) {
            IsServer = isServer;
            MessageHandlers.Add(new HeartBeatMessageHandler());

            MessageHandlers.Add(new FileTransferHeaderHandler());
            MessageHandlers.Add(new TransferStartHandler());
        }

        protected void PostConstruct() {
            var client = IsServer
                ? ConHost.AwaitSingleConnection(Client.Port + 1, Client.CertificatePath)
                : ConHost.Connect(Client.Hostname, Client.Port + 1, Client.CertificatePath);

            FileTransferMessageLoop = new FileTransferMessageLoop(client, this);
            FileTransferMessageLoop.Start();

            _fthIndex = IsServer ? 0 : 1;
            MessageStart = IsServer ? 0 : 1;
        }

        /// <summary>
        /// Tries to queue a file transfer.
        /// Fails when
        ///   - the transfer is already queued or in progress
        ///   - the bucket it references doesn't exist
        ///   - the file that should be sent (if sending) doesn't exist
        /// </summary>
        /// <param name="fth">The transfer to queue.</param>
        /// <param name="fromRemote">Used by the <see cref="FileTransferHeaderHandler"/>. If set, no index is assigned</param>
        /// <returns>Whether the transfer was queued, and it's index if so.</returns>
        public (bool valid, long index) QueueFileTransfer(FileTransferHeader fth, bool fromRemote = false) {
            var bckt = GetBucketList().Where(b => b.Guid.Equals(fth.BucketGuid)).ToList();

            var valid = !PendingFileTransfers.Contains(fth) &&
                        !ActiveFileTransfers.Contains(fth) &&
                        bckt.Count() == 1 &&
                        (fth.ServerSend != IsServer || File.Exists(bckt.First().MakeFilepathAbsolute(fth.Filename)));

            if (!valid) return (false, -1);

            if (!fromRemote) {
                fth.Index = _fthIndex;
                _fthIndex += _fthIndexStep;
            }

            PendingFileTransfers.Add(fth);
            return (true, fth.Index);
        }

        // used to start a transfer that was queued beforehand.
        public bool StartFileTransfer(long index) {
            var head = PendingFileTransfers.First(fth => fth.Index == index);
            PendingFileTransfers.Remove(head);
            ActiveFileTransfers.Add(head);

            if (IsServer) {
                if (head.ServerSend)
                    FileTransferMessageLoop.SendFileData(head);
                else {
                    var m = new Packet(Signal.Type.Control, new[] {
                        (byte) Signal.Codes.TransferStart
                    }.Concat(BitConverter.GetBytes(head.Index)).ToArray()).ToMsg();

                    PostMessage(m);
                }
            } else { return SendTransferHeader(head); }

            return true;
        }

        private bool SendTransferHeader(FileTransferHeader head) {
            var str = new MemoryStream();
            var ser = new XmlSerializer(typeof(FileTransferHeader));
            ser.Serialize(str, head);
            var data = str.ToArray();

            var m = new Packet(Signal.Type.FileTransferHeader,
                    new[] {(byte) Signal.Codes.Ack}.Concat(data).ToArray())
                .ToMsg();
            PostMessage(m);

            m.ResponseHandle.WaitOne();
            return (Signal.Codes) m.Response.Data[0] == Signal.Codes.Ack;
        }

        // Used to get bucket information for sending and receiving
        public abstract List<StorageBucket> GetBucketList();
    }
}
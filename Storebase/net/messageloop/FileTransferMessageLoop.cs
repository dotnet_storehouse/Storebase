﻿using System;
using System.IO;
using System.Linq;
using Storebase.net.@base;
using Storebase.net.messageloop.handler;
using Storebase.store;
using Storebase.util;

namespace Storebase.net.messageloop {
    public class FileTransferMessageLoop : BasicMessageLoop {
        internal readonly StorehouseMessageLoop Parent;
        private readonly int _segmentSize;

        public FileTransferMessageLoop(ITcpClient client, StorehouseMessageLoop parent, int segSizeOverride = 1048576) :
            base(client, "FTML @ " + (parent.IsServer ? "server" : "client")) {
            Parent = parent;
            MessageStart = Parent.IsServer ? 0 : 1;
            _segmentSize = segSizeOverride;

            MessageHandlers.Add(new TransferFinishHandler());
        }

        internal void SendFileData(FileTransferHeader head) {
            var bucket = Parent.GetBucketList().First(b => b.Guid.Equals(head.BucketGuid));
            var file = bucket.MakeFilepathAbsolute(head.Filename);

            var data = Parent.File.ReadAllBytes(file);
            var segments = data.Split(_segmentSize);

            var segNum = 0;

            foreach (var segment in segments) {
                var m = new Packet(Signal.Type.FileTransferData,
                    BitConverter.GetBytes(head.Index) // always a long
                        .Concat(BitConverter.GetBytes(segNum)) // always an integer
                        .Concat(BitConverter.GetBytes(segment.Length)) // also an integer
                        .Concat(segment) // n number of bytes
                        .ToArray()).ToMsg();
                segNum++;

                PostMessage(m);
                // todo: enforce bandwidth limit by waiting here or in the send loop 
            }

            var msg = new Packet(Signal.Type.Control,
                    new[] {(byte) Signal.Codes.TransferFinished}
                        .Concat(BitConverter.GetBytes(head.Index))
                        .Concat(BitConverter.GetBytes(segNum)).ToArray())
                .ToMsg();

            msg.OnResponse += (_, message) => {
                var ret = (Signal.Codes) message.Response.Data[0];

                // ReSharper disable once SwitchStatementMissingSomeCases
                switch (ret) {
                    case Signal.Codes.Ack:
                        Parent.ActiveFileTransfers.Remove(head);
                        break;
                    case Signal.Codes.ErrorSegmentsLostAndDiscarded:
                        SendFileData(head); // full retry
                        break;
                    case Signal.Codes.ErrorSegmentsLostButRecoverable:
                        // todo: Only sent packets that are missing
                        break;
                }
            };

            PostMessage(msg);
        }

        protected override void OnIncomingMessage(ref Message m, bool wasHandeled) {
            if (wasHandeled) return;

            var data = m.Request.Data;

            var index = BitConverter.ToInt64(data, 0);
            var segNum = BitConverter.ToInt32(data, sizeof(long));
            var segLength = BitConverter.ToInt32(data, sizeof(long) + sizeof(int));
            var segData = data.Skip(sizeof(long) + sizeof(int) * 2).ToArray();

            var head = Parent.ActiveFileTransfers.First(fth => fth.Index == index);
            head.Segments.Add((segNum, segData));

            m.Response = new Packet(Signal.Type.Control, Signal.Codes.Ack);
        }
    }
}
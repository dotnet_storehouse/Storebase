﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Storebase.net.@base;

namespace Storebase.net.messageloop {
    public abstract class BasicMessageLoop {
        protected readonly ITcpClient Client;
        private readonly Thread _workerThread;

        private readonly Queue<Message> _outgoingMessages = new Queue<Message>();
        private readonly List<Message> _awaitingReply = new List<Message>();
        protected readonly List<IMessageHandler> MessageHandlers = new List<IMessageHandler>();

        public bool IsRunning => _workerThread.IsAlive;

        protected long MessageIndex = 0;
        private const long MessageStep = 2;
        protected long MessageStart = 0;

        protected BasicMessageLoop(ITcpClient client, string threadname = "Basic Message Loop") {
            Client = client;
            _workerThread = new Thread(Run);
            _workerThread.Name = threadname;
        }

        ~BasicMessageLoop() { Stop(); }

        public void Start() {
            MessageIndex = MessageStart;
            _workerThread.Start();
        }

        public void Stop() {
            _workerThread.Abort();
            Client.Close();
        }

        public long PostMessage(Message m) {
            var id = MessageIndex;
            m.Request.MessageId = MessageIndex;
            _outgoingMessages.Enqueue(m);
            MessageIndex += MessageStep;
            return id;
        }

        private void Run() {
            while (true) {
                if (Client != null)
                    lock (Client) {
                        if (!Client.Online) break;

                        if (Client.Available > 0) {
                            var pn = Client.ReadPacket();
                            if (!pn.HasValue) break;
                            var p = pn.Value;

                            if (p.MessageId % MessageStep == MessageStart) {
                                var req = _awaitingReply.FirstOrDefault(msg => msg.Request.MessageId == p.MessageId);
                                if (req != null) {
                                    req.Response = p;
                                    _awaitingReply.Remove(req);
                                }
                            } else {
                                var m = p.ToMsg();
                                var handeled = false;

                                if (m.Request.Data.Length > 0)
                                    foreach (var h in MessageHandlers) {
                                        // ReSharper disable once InvertIf
                                        if (m.Request.Type == (byte) h.HandeledType &&
                                            (m.Request.Type != (byte) Signal.Type.Control ||
                                             m.Request.Data[0] == (byte) h.HandeledCode)) {
                                            h.OnMessage(ref m, this);
                                            handeled = true;
                                        }
                                    }

                                OnIncomingMessage(ref m, handeled);

                                var resp = m.Response;
                                resp.MessageId = m.Request.MessageId;
                                Client.SendPacket(resp);
                            }
                        }

                        while (_outgoingMessages.Count > 0) {
                            var m = _outgoingMessages.Dequeue();
                            Client.SendPacket(m.Request);
                            _awaitingReply.Add(m);
                        }
                    }

                Thread.Sleep(100);
            }

            OnDisconnect();
        }

        protected virtual void OnIncomingMessage(ref Message m, bool wasHandeled) {
            if (wasHandeled) return;
            m.Response = new Packet(Signal.Type.Control, Signal.Codes.Error);
        }

        protected virtual void OnDisconnect() { }
    }
}
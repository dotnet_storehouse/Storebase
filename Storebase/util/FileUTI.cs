﻿using System.IO;

namespace Storebase.util {
    public interface IFileTestInterface {
        void WriteAllBytes(string filename, byte[] bytes);
        byte[] ReadAllBytes(string filename);
        bool Exists(string filename);
    }

    public class FileTestInterface : IFileTestInterface {
        public void WriteAllBytes(string filename, byte[] bytes) {
            Directory.CreateDirectory(Path.GetDirectoryName(filename));
            File.WriteAllBytes(filename, bytes);
        }

        public byte[] ReadAllBytes(string filename) {
            return File.ReadAllBytes(filename);
        }

        public bool Exists(string filename) {
            return File.Exists(filename);
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;

namespace Storebase.util {
    public static class ExtensionMethods {

        public static IEnumerable<T[]> Split<T>(this T[] arr, int segLength) {
            var index = 0;
            while (true) {
                yield return arr.Skip(index * segLength).Take(segLength).ToArray();
                index++;
                if (index * segLength >= arr.Length) break;
            }
        }
        
    }
}
﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace Storebase.util {
    public static class FileUtils {

        // ReSharper disable once InconsistentNaming
        public static string GetMD5(string filename) {
            using (var str = File.OpenRead(filename))
            using (var md5 = SHA512.Create()) {
                var hash = md5.ComputeHash(str);
                return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
            }
        }
    }
}
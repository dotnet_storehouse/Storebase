﻿using System;

namespace Storebase.args {
    public class Argument<T> {

        public string LongName;
        public string ShortName;
        
        public Argument(string longName, string shortName = "") {
            LongName = longName.Replace(" ", "-").ToLowerInvariant();
            ShortName = shortName.Replace(" ", "-").ToLowerInvariant();
        }

        public T Fetch(string[] args, T @default = default(T)) {
            foreach (var s in args) {
                if (s.StartsWith($"--{LongName}=", StringComparison.InvariantCultureIgnoreCase) ||
                    s.StartsWith($"-{ShortName}=", StringComparison.InvariantCultureIgnoreCase)) {
                    return (T) Convert.ChangeType(s.Substring(s.IndexOf("=", StringComparison.InvariantCulture) + 1), typeof(T));
                }
            }
            return @default;
        }
        
    }
}